var localStrategy = require('passport-local').Strategy;
var mysql = require('mysql'),
    psw = require('.././passport/encryptPassword'),
    config = require('.././models/db');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });

    passport.use(new localStrategy({
        passReqToCallback: true
    }, function(req, username, password, done) {
        var db = mysql.createConnection(config);

        db.connect();
            db.query('SELECT * FROM login WHERE user = ?', username, function(err, rows) {
                if(err) throw err

        db.end();

                if(rows.length > 0) {
                    var user = rows[0],
                        pass = psw.encryptPass(req.body.password);

                    if(pass.toLowerCase() === user.password.toLowerCase()) {
                        return done(null, {
                            id: user.id,
                            username: user.user,
                            rol: user.rol
                        });
                    }
                }
                return done(null, {username:null}, req.flash('authMsg', 'Username o Clave incorrectos.'));
            });
    }));
};