var crypto = require('crypto');

module.exports = {
    encryptPass : function(password) {

        var psw = crypto.createHash('md5')
            .update(password)
                .digest('hex');

        return psw;
    }

}