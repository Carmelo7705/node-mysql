
var express = require('express');
var router = express.Router();
var passport = require('passport');

var controller = require('.././controllers'); //No colocamos el nombre del archivo porque es index.js
//y se sobreentiende.

/*Home Page */
router.get('/', controller.HomeController.index); //Gracias al archivo de controladores, tenemos acceso
//a todos mediante un solo archivo que los recorre.

/*Contact Page */
router.get('/contacts', controller.ContactsController.index);
/*Register Page  and Register POST */
router.get('/register', controller.HomeController.register);
router.post('/register', controller.HomeController.postRegister);

/*Login POST*/
/*router.post('/signin', passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/register',
    failureFlash: true
}));*/

router.post('/signin', passport.authenticate('local'), function(req, res) {
   if(req.user.username) {
        if(req.user.rol > 0) {
            return res.redirect('/');
        } else {
            return res.redirect('/dashboard');
        }
   } else {
       return res.redirect('/register');
   }

});

/*Dashboard Page */
router.get('/dashboard', controller.DashboardController.index);

module.exports = router;
