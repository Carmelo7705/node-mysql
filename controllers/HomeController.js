var mysql = require('mysql'),
    psw = require('.././passport/encryptPassword'),
    config = require('.././models/db');

module.exports = {

    index: function(req, res, next) {

        var db = mysql.createConnection(config);

        db.connect();

        db.query('SELECT * FROM products', (err, rows) => {
            let data = {
                title: 'Lista de productos',
                data: rows
            }
        res.render('shop/index', data);
        db.end();
        });
    },

    register: function(req, res, next) {
        return res.render('shop/register', {message: req.flash('success')});
    },

    postRegister: function(req, res, next) {
        
        var password = psw.encryptPass(req.body.password),

            user =  {
                user: req.body.username,
                password: password,
                rol: '1'
            }

            var db = mysql.createConnection(config);

            db.connect();

            db.query('INSERT INTO login SET ?', user, function(err, rows) {
                if(err) throw err;
        
                db.end();
            });
            req.flash('success', 'Te has registrado correctamente!');
            return res.redirect('/register');
    }
    
}