var fs = require('fs'),
    path = require('path'),

    files = fs.readdirSync(__dirname); //Abró el directorio actual, de manera sincrona.

    files.forEach(function(file) { //Recorremos el directorio
        var fileName = path.basename(file, '.js'); //Le colocamos cualquier nombre de arcvivo, pero
        //que sea de extensión .js

        if(fileName !== 'index') { //Todos los archivos que sean distinto a este "index.js".
            exports[fileName] = require('./'+ fileName); //Lo exportamos, para usar en cualquier lugar.
        }
    });
